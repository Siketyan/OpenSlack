﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenSlack
{
	public class GroupResult : Result
	{
		[JsonProperty("group")]
		public Group Group { get; set; }
	}
}
