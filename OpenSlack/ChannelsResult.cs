﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace OpenSlack
{
    public class ChannelsResult : Result
    {
        [JsonProperty("channels")]
        public List<Channel> Channels { get; set; }
    }
}