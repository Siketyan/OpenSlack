﻿using Newtonsoft.Json;

namespace OpenSlack
{
    public class DndResult : Result
    {
        /// <summary>
        /// This will be null if called by SetSnooze method.
        /// </summary>
        [JsonProperty("dnd_enabled")]
        public bool IsEnabled { get; set; }

        /// <summary>
        /// This will be null if called by SetSnooze method.
        /// </summary>
        [JsonProperty("next_dnd_start_ts")]
        public long NextStart { get; set; }

        /// <summary>
        /// This will be null if called by SetSnooze method.
        /// </summary>
        [JsonProperty("next_dnd_end_ts")]
        public long NextEnd { get; set; }

        /// <summary>
        /// If called the method without current user, this will be null.
        /// </summary>
        [JsonProperty("snooze_enabled")]
        public bool IsSnoozeEnabled { get; set; }
        
        /// <summary>
        /// This will be setted only by GetDndInfo and SetSnooze method.
        /// If IsSnoozeEnabled is false or called the method without current user,
        /// then this will be null.
        /// </summary>
        [JsonProperty("snooze_endtime")]
        public long SnoozeEnd { get; set; }

        /// <summary>
        /// This will be setted only by GetDndInfo and SetSnooze method.
        /// If IsSnoozeEnabled is false or called the method without current user,
        /// then this will be null.
        /// </summary>
        [JsonProperty("snooze_remaining")]
        public int SnoozeRemaining { get; set; }    
    }
}