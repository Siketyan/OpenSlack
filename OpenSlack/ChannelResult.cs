﻿using Newtonsoft.Json;

namespace OpenSlack
{
    public class ChannelResult : Result
    {
        [JsonProperty("channel")]
        public Channel Channel { get; set; }
    }
}
