﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenSlack
{
	public class GroupCloseResult : Result
	{
		[JsonProperty("no_op")]
		public bool IsNoOp { get; set; }

		[JsonProperty("already_closed")]
		public bool AlreadyClosed { get; set; }
	}
}
