﻿using Newtonsoft.Json;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using static System.Text.Encoding;

namespace OpenSlack
{
    public class API
    {
        public string Token { get; private set; }
        public User You { get; private set; }

        public API(string Token)
        {
            this.Token = Token;
            You = Test();
        }

        #region internal methods
        private T SendRequest<T>(string Method, string Args = "") where T : Result
        {
            HttpWebRequest Request
                = (HttpWebRequest)WebRequest.Create(
                    "https://slack.com/api/" + Method + "?token=" + Token + "&" + Args
                  );
            Request.Method = "GET";

            HttpWebResponse APIResponse = (HttpWebResponse)Request.GetResponse();
            Stream ResponseStream = APIResponse.GetResponseStream();
            StreamReader ResponseReader = new StreamReader(ResponseStream, GetEncoding("UTF-8"));

            string Json = ResponseReader.ReadToEnd();
            T Response = JsonConvert.DeserializeObject<T>(Json);
            Response.Json = Json;
            if (!Response.IsOK) throw new SlackException(Response.Error);

            ResponseReader.Close();
            ResponseStream.Close();
            APIResponse.Close();

            return Response;
        }

        private async Task<T> SendRequestAsync<T>(string Method, string Args = "") where T : Result
        {
            T Result = await Task.Run(() =>
            {
                HttpWebRequest Request
                    = (HttpWebRequest)WebRequest.Create(
                        "https://slack.com/api/" + Method + "?token=" + Token + "&" + Args
                      );
                Request.Method = "GET";

                HttpWebResponse APIResponse = (HttpWebResponse)Request.GetResponse();
                Stream ResponseStream = APIResponse.GetResponseStream();
                StreamReader ResponseReader = new StreamReader(ResponseStream, GetEncoding("UTF-8"));

                string Json = ResponseReader.ReadToEnd();
                T Response = JsonConvert.DeserializeObject<T>(Json);
                Response.Json = Json;
                if (!Response.IsOK) throw new SlackException(Response.Error);

                ResponseReader.Close();
                ResponseStream.Close();
                APIResponse.Close();

                return Response;
            });

            return Result;
        }
        #endregion

        #region auth.test
        /// <summary>
        /// This method checks authentication and tells you who you are.
        /// </summary>
        /// <returns>You</returns>
        public User Test()
        {
            return SendRequest<User>("auth.test", "");
        }
        #endregion
        #region auth.revoke
        /// <summary>
        /// This method revokes an access token.
        /// Use it when you no longer need a token.
        /// For example, with a Sign In With Slack app, call this to log a user out.
        /// </summary>
        /// <returns>Revoking Result</returns>
        public RevokeResult Revoke()
        {
            return SendRequest<RevokeResult>("auth.revoke", "");
        }

        /// <summary>
        /// This method revokes an access token.
        /// Use it when you no longer need a token.
        /// For example, with a Sign In With Slack app, call this to log a user out.
        /// </summary>
        /// <returns>Revoking Result</returns>
        public async Task<RevokeResult> RevokeAsync()
        {
            return await SendRequestAsync<RevokeResult>("auth.revoke", "");
        }
        #endregion

        #region bots.info
        /// <summary>
        /// This method returns information about a bot user.
        /// </summary>
        /// <param name="BotID">Bot user's ID to get info on</param>
        /// <returns>Informations about the bot user</returns>
        public BotResult GetBot(string BotID)
        {
            return SendRequest<BotResult>("bots.info", "bot=" + BotID);
        }

        /// <summary>
        /// This method returns information about a bot user.
        /// </summary>
        /// <param name="BotID">Bot user's ID to get info on</param>
        /// <returns>Informations about the bot user</returns>
        public async Task<BotResult> GetBotAsync(string BotID)
        {
            return await SendRequestAsync<BotResult>("bots.info", "bot=" + BotID);
        }
        #endregion

        #region channels.archive
        /// <summary>
        /// This method archives a channel.
        /// (Requires scope: channels:write)
        /// </summary>
        /// <param name="Ch">A channel to archive</param>
        /// <returns>Result of archiving</returns>
        public Result ArchiveChannel(Channel Ch)
        {
            return SendRequest<Result>("channels.archive", "channel=" + Ch.ID);
        }

        /// <summary>
        /// This method archives a channel.
        /// (Requires scope: channels:write)
        /// </summary>
        /// <param name="ChannelID">ID of channel to archive</param>
        /// <returns>Result of archiving</returns>
        public Result ArchiveChannel(string ChannelID)
        {
            return SendRequest<Result>("channels.archive", "channel=" + ChannelID);
        }

        /// <summary>
        /// This method archives a channel.
        /// (Requires scope: channels:write)
        /// </summary>
        /// <param name="Ch">A channel to archive</param>
        /// <returns>Result of archiving</returns>
        public async Task<Result> ArchiveChannelAsync(Channel Ch)
        {
            return await SendRequestAsync<Result>("channels.archive", "channel=" + Ch.ID);
        }

        /// <summary>
        /// This method archives a channel.
        /// (Requires scope: channels:write)
        /// </summary>
        /// <param name="ChannelID">ID of channel to archive</param>
        /// <returns>Result of archiving</returns>
        public async Task<Result> ArchiveChannelAsync(string ChannelID)
        {
            return await SendRequestAsync<Result>("channels.archive", "channel=" + ChannelID);
        }
        #endregion
        #region channels.create
        /// <summary>
        /// This method is used to create a channel.
        /// (Requires scope: channels:write)
        /// </summary>
        /// <param name="Name">
        /// Channel names can only contain lowercase letters, numbers, hyphens,
        /// and underscores, and must be 21 characters or less. 
        /// We will validate the submitted channel name and modify it to meet the above criteria.
        /// When calling this method, we recommend storing the channel's name value that is returned in the response.
        /// </param>
        /// <returns>Created channel</returns>
        public ChannelResult CreateChannel(string Name)
        {
            return SendRequest<ChannelResult>("channels.create", "name=" + Name);
        }

        /// <summary>
        /// This method is used to create a channel.
        /// (Requires scope: channels:write)
        /// </summary>
        /// <param name="Name">
        /// Channel names can only contain lowercase letters, numbers, hyphens,
        /// and underscores, and must be 21 characters or less. 
        /// We will validate the submitted channel name and modify it to meet the above criteria.
        /// When calling this method, we recommend storing the channel's name value that is returned in the response.
        /// </param>
        /// <returns>Created channel</returns>
        public async Task<ChannelResult> CreateChannelAsync(string Name)
        {
            return await SendRequestAsync<ChannelResult>("channels.create", "name=" + Name);
        }
        #endregion
        #region channels.history
        /// <summary>
        /// This method returns a portion of message events from the specified channel.
        /// To read the entire history for a channel, call the method with no latest or oldest arguments,
        /// and then continue paging using the instructions below.
        /// </summary>
        /// <param name="Ch">Channel to fetch history for.</param>
        /// <param name="Latest">End of time range of messages to include in results.</param>
        /// <param name="Oldest">Start of time range of messages to include in results.</param>
        /// <param name="Inclusive">Include messages with latest or oldest timestamp in results.</param>
        /// <param name="Count">Number of messages to return, between 1 and 1000.</param>
        /// <param name="Unreads">Include UnreadsDisplay in the output?</param>
        /// <returns>A portion of message events from the specified channel.</returns>
        public MessagesResult GetHistory(Channel Ch, string Latest = "now", string Oldest = "0",
                                         bool Inclusive = false, int Count = 100, bool Unreads = false)
        {
            int InclusiveRaw = (Inclusive) ? 1 : 0;
            int UnreadsRaw = (Unreads) ? 1 : 0;

            return SendRequest<MessagesResult>(
                "channels.history", "channel=" + Ch.ID + "&latest=" + Latest + "&oldest=" + Oldest
                    + "&inclusive=" + InclusiveRaw + "&count=" + Count + "&unreads=" + UnreadsRaw
            );
        }

        /// <summary>
        /// This method returns a portion of message events from the specified channel.
        /// To read the entire history for a channel, call the method with no latest or oldest arguments,
        /// and then continue paging using the instructions below.
        /// </summary>
        /// <param name="ChannelID">ID of channel to fetch history for.</param>
        /// <param name="Latest">End of time range of messages to include in results.</param>
        /// <param name="Oldest">Start of time range of messages to include in results.</param>
        /// <param name="Inclusive">Include messages with latest or oldest timestamp in results.</param>
        /// <param name="Count">Number of messages to return, between 1 and 1000.</param>
        /// <param name="Unreads">Include UnreadsDisplay in the output?</param>
        /// <returns>A portion of message events from the specified channel.</returns>
        public MessagesResult GetHistory(string ChannelID, string Latest = "now", string Oldest = "0",
                                         bool Inclusive = false, int Count = 100, bool Unreads = false)
        {
            int InclusiveRaw = (Inclusive) ? 1 : 0;
            int UnreadsRaw = (Unreads) ? 1 : 0;

            return SendRequest<MessagesResult>(
                "channels.history", "channel=" + ChannelID + "&latest=" + Latest + "&oldest=" + Oldest
                    + "&inclusive=" + InclusiveRaw + "&count=" + Count + "&unreads=" + UnreadsRaw
            );
        }

        /// <summary>
        /// This method returns a portion of message events from the specified channel.
        /// To read the entire history for a channel, call the method with no latest or oldest arguments,
        /// and then continue paging using the instructions below.
        /// </summary>
        /// <param name="Ch">Channel to fetch history for.</param>
        /// <param name="Latest">End of time range of messages to include in results.</param>
        /// <param name="Oldest">Start of time range of messages to include in results.</param>
        /// <param name="Inclusive">Include messages with latest or oldest timestamp in results.</param>
        /// <param name="Count">Number of messages to return, between 1 and 1000.</param>
        /// <param name="Unreads">Include UnreadsDisplay in the output?</param>
        /// <returns>A portion of message events from the specified channel.</returns>
        public async Task<MessagesResult> GetHistoryAsync(
            Channel Ch, string Latest = "now", string Oldest = "0",
            bool Inclusive = false, int Count = 100, bool Unreads = false
        )
        {
            int InclusiveRaw = (Inclusive) ? 1 : 0;
            int UnreadsRaw = (Unreads) ? 1 : 0;

            return await SendRequestAsync<MessagesResult>(
                "channels.history", "channel=" + Ch.ID + "&latest=" + Latest + "&oldest=" + Oldest
                    + "&inclusive=" + InclusiveRaw + "&count=" + Count + "&unreads=" + UnreadsRaw
            );
        }

        /// <summary>
        /// This method returns a portion of message events from the specified channel.
        /// To read the entire history for a channel, call the method with no latest or oldest arguments,
        /// and then continue paging using the instructions below.
        /// </summary>
        /// <param name="ChannelID">ID of channel to fetch history for.</param>
        /// <param name="Latest">End of time range of messages to include in results.</param>
        /// <param name="Oldest">Start of time range of messages to include in results.</param>
        /// <param name="Inclusive">Include messages with latest or oldest timestamp in results.</param>
        /// <param name="Count">Number of messages to return, between 1 and 1000.</param>
        /// <param name="Unreads">Include UnreadsDisplay in the output?</param>
        /// <returns>A portion of message events from the specified channel.</returns>
        public async Task<MessagesResult> GetHistoryAsync(
            string ChannelID, string Latest = "now", string Oldest = "0",
            bool Inclusive = false, int Count = 100, bool Unreads = false
        )
        {
            int InclusiveRaw = (Inclusive) ? 1 : 0;
            int UnreadsRaw = (Unreads) ? 1 : 0;

            return await SendRequestAsync<MessagesResult>(
                "channels.history", "channel=" + ChannelID + "&latest=" + Latest + "&oldest=" + Oldest
                    + "&inclusive=" + InclusiveRaw + "&count=" + Count + "&unreads=" + UnreadsRaw
            );
        }
        #endregion
        #region channels.info
        /// <summary>
        /// This method returns information about a team channel.
        /// </summary>
        /// <param name="ID">ID of channel to get info on</param>
        /// <returns>Information about a team channel.</returns>
        public ChannelResult GetChannel(string ID)
        {
            return SendRequest<ChannelResult>("channels.info", "channel=" + ID);
        }

        /// <summary>
        /// This method returns information about a team channel.
        /// </summary>
        /// <param name="ID">ID of channel to get info on</param>
        /// <returns>Information about a team channel.</returns>
        public async Task<ChannelResult> GetChannelAsync(string ID)
        {
            return await SendRequestAsync<ChannelResult>("channels.info", "channel=" + ID);
        }
        #endregion
        #region channels.invite
        /// <summary>
        /// This method is used to invite a user to a channel.
        /// The calling user must be a member of the channel.
        /// </summary>
        /// <param name="Ch">Channel to invite user to.</param>
        /// <param name="Us">User to invite to channel.</param>
        /// <returns>Information about the channel after inviting.</returns>
        public ChannelResult InviteToChannel(Channel Ch, User Us)
        {
            return SendRequest<ChannelResult>("channels.invite", "channel=" + Ch.ID + "&user=" + Us.ID);
        }

        /// <summary>
        /// This method is used to invite a user to a channel.
        /// The calling user must be a member of the channel.
        /// </summary>
        /// <param name="ChannelID">ID of channel to invite user to.</param>
        /// <param name="UserID">ID of user to invite to channel.</param>
        /// <returns>Information about the channel after inviting.</returns>
        public ChannelResult InviteToChannel(string ChannelID, string UserID)
        {
            return SendRequest<ChannelResult>("channels.invite", "channel=" + ChannelID + "&user=" + UserID);
        }

        /// <summary>
        /// This method is used to invite a user to a channel.
        /// The calling user must be a member of the channel.
        /// </summary>
        /// <param name="Ch">Channel to invite user to.</param>
        /// <param name="Us">User to invite to channel.</param>
        /// <returns>Information about the channel after inviting.</returns>
        public async Task<ChannelResult> InviteToChannelAsync(Channel Ch, User Us)
        {
            return await SendRequestAsync<ChannelResult>("channels.invite", "channel=" + Ch.ID + "&user=" + Us.ID);
        }

        /// <summary>
        /// This method is used to invite a user to a channel.
        /// The calling user must be a member of the channel.
        /// </summary>
        /// <param name="ChannelID">ID of channel to invite user to.</param>
        /// <param name="UserID">ID of user to invite to channel.</param>
        /// <returns>Information about the channel after inviting.</returns>
        public async Task<ChannelResult> InviteToChannelAsync(string ChannelID, string UserID)
        {
            return await SendRequestAsync<ChannelResult>("channels.invite", "channel=" + ChannelID + "&user=" + UserID);
        }
        #endregion
        #region channels.join
        /// <summary>
        /// This method is used to join a channel.
        /// If the channel does not exist, it is created.
        /// </summary>
        /// <param name="Ch">Channel to join.</param>
        /// <returns>Information about joined channel.</returns>
        public ChannelResult JoinToChannel(Channel Ch)
        {
            return SendRequest<ChannelResult>("channels.join", "name=" + Ch.Name);
        }

        /// <summary>
        /// This method is used to join a channel.
        /// If the channel does not exist, it is created.
        /// </summary>
        /// <param name="ChannelName">Name of channel to join.</param>
        /// <returns>Information about joined channel.</returns>
        public ChannelResult JoinToChannel(string ChannelName)
        {
            return SendRequest<ChannelResult>("channels.join", "name=" + ChannelName);
        }

        /// <summary>
        /// This method is used to join a channel.
        /// If the channel does not exist, it is created.
        /// </summary>
        /// <param name="Ch">Channel to join.</param>
        /// <returns>Information about joined channel.</returns>
        public async Task<ChannelResult> JoinToChannelAsync(Channel Ch)
        {
            return await SendRequestAsync<ChannelResult>("channels.join", "name=" + Ch.Name);
        }

        /// <summary>
        /// This method is used to join a channel.
        /// If the channel does not exist, it is created.
        /// </summary>
        /// <param name="ChannelName">Name of channel to join.</param>
        /// <returns>Information about joined channel.</returns>
        public async Task<ChannelResult> JoinToChannelAsync(string ChannelName)
        {
            return await SendRequestAsync<ChannelResult>("channels.join", "name=" + ChannelName);
        }
        #endregion
        #region channels.kick
        /// <summary>
        /// This method allows a user to remove another member from a team channel.
        /// </summary>
        /// <param name="Ch">Channel to remove user from.</param>
        /// <param name="Us">User to remove from channel.</param>
        /// <returns>Result of removing.</returns>
        public Result KickFromChannel(Channel Ch, User Us)
        {
            return SendRequest<Result>("channels.kick", "channel=" + Ch.ID + "&user=" + Us.ID);
        }

        /// <summary>
        /// This method allows a user to remove another member from a team channel.
        /// </summary>
        /// <param name="ChannelID">ID of channel to remove user from.</param>
        /// <param name="UserID">ID of user to remove from channel.</param>
        /// <returns>Result of removing.</returns>
        public Result KickFromChannel(string ChannelID, string UserID)
        {
            return SendRequest<Result>("channels.kick", "channel=" + ChannelID + "&user=" + UserID);
        }

        /// <summary>
        /// This method allows a user to remove another member from a team channel.
        /// </summary>
        /// <param name="Ch">Channel to remove user from.</param>
        /// <param name="Us">User to remove from channel.</param>
        /// <returns>Result of removing.</returns>
        public async Task<Result> KickFromChannelAsync(Channel Ch, User Us)
        {
            return await SendRequestAsync<Result>("channels.kick", "channel=" + Ch.ID + "&user=" + Us.ID);
        }

        /// <summary>
        /// This method allows a user to remove another member from a team channel.
        /// </summary>
        /// <param name="ChannelID">ID of channel to remove user from.</param>
        /// <param name="UserID">ID of user to remove from channel.</param>
        /// <returns>Result of removing.</returns>
        public async Task<Result> KickFromChannelAsync(string ChannelID, string UserID)
        {
            return await SendRequestAsync<Result>("channels.kick", "channel=" + ChannelID + "&user=" + UserID);
        }
        #endregion
        #region channels.leave
        /// <summary>
        /// This method is used to leave a channel.
        /// </summary>
        /// <param name="Ch">Channel to leave</param>
        /// <returns>Result of leaving</returns>
        public Result LeaveFromChannel(Channel Ch)
        {
            return SendRequest<Result>("channels.leave", "channel=" + Ch.ID);
        }

        /// <summary>
        /// This method is used to leave a channel.
        /// </summary>
        /// <param name="ChannelID">Channel to leave</param>
        /// <returns>Result of leaving</returns>
        public Result LeaveFromChannel(string ChannelID)
        {
            return SendRequest<Result>("channels.leave", "channel=" + ChannelID);
        }

        /// <summary>
        /// This method is used to leave a channel.
        /// </summary>
        /// <param name="Ch">Channel to leave</param>
        /// <returns>Result of leaving</returns>
        public async Task<Result> LeaveFromChannelAsync(Channel Ch)
        {
            return await SendRequestAsync<Result>("channels.leave", "channel=" + Ch.ID);
        }

        /// <summary>
        /// This method is used to leave a channel.
        /// </summary>
        /// <param name="ChannelID">Channel to leave</param>
        /// <returns>Result of leaving</returns>
        public async Task<Result> LeaveFromChannelAsync(string ChannelID)
        {
            return await SendRequestAsync<Result>("channels.leave", "channel=" + ChannelID);
        }
        #endregion
        #region channels.list
        /// <summary>
        /// This method returns a list of all channels in the team.
        /// This includes channels the caller is in, channels they are not currently in,
        /// and archived channels but does not include private channels.
        /// The number of (non-deactivated) members in each channel is also returned.
        /// </summary>
        /// <returns>A list of all channels in the team</returns>
        public ChannelsResult GetChannels()
        {
            return SendRequest<ChannelsResult>("channels.list");
        }

        /// <summary>
        /// This method returns a list of all channels in the team.
        /// This includes channels the caller is in, channels they are not currently in,
        /// and archived channels but does not include private channels.
        /// The number of (non-deactivated) members in each channel is also returned.
        /// </summary>
        /// <returns>A list of all channels in the team</returns>
        public async Task<ChannelsResult> GetChannelsAsync()
        {
            return await SendRequestAsync<ChannelsResult>("channels.list");
        }
        #endregion
        #region channels.mark
        /// <summary>
        /// This method moves the read cursor in a channel.
        /// </summary>
        /// <param name="Ch">Channel to set reading cursor in.</param>
        /// <param name="Timestamp">Timestamp of the most recently seen message.</param>
        /// <returns>Result of marking.</returns>
        public Result MarkRead(Channel Ch, string Timestamp)
        {
            return SendRequest<Result>("channels.mark", "channel=" + Ch.ID + "&ts=" + Timestamp);
        }

        /// <summary>
        /// This method moves the read cursor in a channel.
        /// </summary>
        /// <param name="ChannelID">ID of channel to set reading cursor in.</param>
        /// <param name="Timestamp">Timestamp of the most recently seen message.</param>
        /// <returns>Result of marking.</returns>
        public Result MarkRead(string ChannelID, string Timestamp)
        {
            return SendRequest<Result>("channels.mark", "channel=" + ChannelID + "&ts=" + Timestamp);
        }

        /// <summary>
        /// This method moves the read cursor in a channel.
        /// </summary>
        /// <param name="Ch">Channel to set reading cursor in.</param>
        /// <param name="Timestamp">Timestamp of the most recently seen message.</param>
        /// <returns>Result of marking.</returns>
        public async Task<Result> MarkReadAsync(Channel Ch, string Timestamp)
        {
            return await SendRequestAsync<Result>("channels.mark", "channel=" + Ch.ID + "&ts=" + Timestamp);
        }

        /// <summary>
        /// This method moves the read cursor in a channel.
        /// </summary>
        /// <param name="ChannelID">ID of channel to set reading cursor in.</param>
        /// <param name="Timestamp">Timestamp of the most recently seen message.</param>
        /// <returns>Result of marking.</returns>
        public async Task<Result> MarkReadAsync(string ChannelID, string Timestamp)
        {
            return await SendRequestAsync<Result>("channels.mark", "channel=" + ChannelID + "&ts=" + Timestamp);
        }
        #endregion
        #region channels.rename
        /// <summary>
        /// This method renames a team channel.
        /// </summary>
        /// <param name="Ch">Channel to rename.</param>
        /// <param name="Name">New name for channel.</param>
        /// <returns>
        /// Result of Renaming and information about the channel after renaming.
        /// (Result.Channel will be filled only ID, Name, and Created.)
        /// </returns>
        public ChannelResult RenameChannel(Channel Ch, string Name)
        {
            return SendRequest<ChannelResult>("channels.rename", "channel=" + Ch.ID + "&name=" + Name);
        }

        /// <summary>
        /// This method renames a team channel.
        /// </summary>
        /// <param name="ChannelID">ID of channel to rename.</param>
        /// <param name="Name">New name for channel.</param>
        /// <returns>
        /// Result of Renaming and information about the channel after renaming.
        /// (Result.Channel will be filled only ID, Name, and Created.)
        /// </returns>
        public ChannelResult RenameChannel(string ChannelID, string Name)
        {
            return SendRequest<ChannelResult>("channels.rename", "channel=" + ChannelID + "&name=" + Name);
        }

        /// <summary>
        /// This method renames a team channel.
        /// </summary>
        /// <param name="Ch">Channel to rename.</param>
        /// <param name="Name">New name for channel.</param>
        /// <returns>
        /// Result of Renaming and information about the channel after renaming.
        /// (Result.Channel will be filled only ID, Name, and Created.)
        /// </returns>
        public async Task<ChannelResult> RenameChannelAsync(Channel Ch, string Name)
        {
            return await SendRequestAsync<ChannelResult>("channels.rename", "channel=" + Ch.ID + "&name=" + Name);
        }

        /// <summary>
        /// This method renames a team channel.
        /// </summary>
        /// <param name="ChannelID">ID of channel to rename.</param>
        /// <param name="Name">New name for channel.</param>
        /// <returns>
        /// Result of Renaming and information about the channel after renaming.
        /// (Result.Channel will be filled only ID, Name, and Created.)
        /// </returns>
        public async Task<ChannelResult> RenameChannelAsync(string ChannelID, string Name)
        {
            return await SendRequestAsync<ChannelResult>("channels.rename", "channel=" + ChannelID + "&name=" + Name);
        }
        #endregion
        #region channels.setPurpose
        /// <summary>
        /// This method is used to change the purpose of a channel.
        /// The calling user must be a member of the channel.
        /// </summary>
        /// <param name="Ch">Channel to set the purpose of.</param>
        /// <param name="Purpose">The new purpose.</param>
        /// <returns>Result of setting the purpose</returns>
        public Result SetChannelPurpose(Channel Ch, string Purpose)
        {
            return SendRequest<Result>("channels.setPurpose", "channel=" + Ch.ID + "&purpose=" + Purpose);
        }

        /// <summary>
        /// This method is used to change the purpose of a channel.
        /// The calling user must be a member of the channel.
        /// </summary>
        /// <param name="ChannelID">ID of channel to set the purpose of.</param>
        /// <param name="Purpose">The new purpose.</param>
        /// <returns>Result of setting the purpose</returns>
        public Result SetChannelPurpose(string ChannelID, string Purpose)
        {
            return SendRequest<Result>("channels.setPurpose", "channel=" + ChannelID + "&purpose=" + Purpose);
        }

        /// <summary>
        /// This method is used to change the purpose of a channel.
        /// The calling user must be a member of the channel.
        /// </summary>
        /// <param name="Ch">Channel to set the purpose of.</param>
        /// <param name="Purpose">The new purpose.</param>
        /// <returns>Result of setting the purpose</returns>
        public async Task<Result> SetChannelPurposeAsync(Channel Ch, string Purpose)
        {
            return await SendRequestAsync<Result>("channels.setPurpose", "channel=" + Ch.ID + "&purpose=" + Purpose);
        }

        /// <summary>
        /// This method is used to change the purpose of a channel.
        /// The calling user must be a member of the channel.
        /// </summary>
        /// <param name="ChannelID">ID of channel to set the purpose of.</param>
        /// <param name="Purpose">The new purpose.</param>
        /// <returns>Result of setting the purpose</returns>
        public async Task<Result> SetChannelPurposeAsync(string ChannelID, string Purpose)
        {
            return await SendRequestAsync<Result>("channels.setPurpose", "channel=" + ChannelID + "&purpose=" + Purpose);
        }
        #endregion
        #region channels.setTopic
        /// <summary>
        /// This method is used to change the topic of a channel.
        /// The calling user must be a member of the channel.
        /// </summary>
        /// <param name="Ch">Channel to set the topic of.</param>
        /// <param name="Topic">The new topic.</param>
        /// <returns>Result of setting the topic</returns>
        public Result SetChannelTopic(Channel Ch, string Topic)
        {
            return SendRequest<Result>("channels.setTopic", "channel=" + Ch.ID + "&topic=" + Topic);
        }

        /// <summary>
        /// This method is used to change the topic of a channel.
        /// The calling user must be a member of the channel.
        /// </summary>
        /// <param name="ChannelID">ID of channel to set the topic of.</param>
        /// <param name="Topic">The new topic.</param>
        /// <returns>Result of setting the topic</returns>
        public Result SetChannelTopic(string ChannelID, string Topic)
        {
            return SendRequest<Result>("channels.setTopic", "channel=" + ChannelID + "&topic=" + Topic);
        }

        /// <summary>
        /// This method is used to change the topic of a channel.
        /// The calling user must be a member of the channel.
        /// </summary>
        /// <param name="Ch">Channel to set the topic of.</param>
        /// <param name="Topic">The new topic.</param>
        /// <returns>Result of setting the topic</returns>
        public async Task<Result> SetChannelTopicAsync(Channel Ch, string Topic)
        {
            return await SendRequestAsync<Result>("channels.setTopic", "channel=" + Ch.ID + "&topic=" + Topic);
        }

        /// <summary>
        /// This method is used to change the topic of a channel.
        /// The calling user must be a member of the channel.
        /// </summary>
        /// <param name="ChannelID">ID of channel to set the topic of.</param>
        /// <param name="Topic">The new topic.</param>
        /// <returns>Result of setting the topic</returns>
        public async Task<Result> SetChannelTopicAsync(string ChannelID, string Topic)
        {
            return await SendRequestAsync<Result>("channels.setTopic", "channel=" + ChannelID + "&topic=" + Topic);
        }
        #endregion
        #region channels.unarchive
        /// <summary>
        /// This Method Unarchives a channel.
        /// The calling user is added to the channel.
        /// </summary>
        /// <param name="Ch">Channel to unarchive.</param>
        /// <returns>Result of unarchiving.</returns>
        public Result UnarchiveChannel(Channel Ch)
        {
            return SendRequest<Result>("channels.unarchive", "channel=" + Ch.ID);
        }

        /// <summary>
        /// This Method Unarchives a channel.
        /// The calling user is added to the channel.
        /// </summary>
        /// <param name="ChannelID">ID of channel to unarchive.</param>
        /// <returns>Result of unarchiving.</returns>
        public Result UnarchiveChannel(string ChannelID)
        {
            return SendRequest<Result>("channels.unarchive", "channel=" + ChannelID);
        }

        /// <summary>
        /// This Method Unarchives a channel.
        /// The calling user is added to the channel.
        /// </summary>
        /// <param name="Ch">Channel to unarchive.</param>
        /// <returns>Result of unarchiving.</returns>
        public async Task<Result> UnarchiveChannelAsync(Channel Ch)
        {
            return await SendRequestAsync<Result>("channels.unarchive", "channel=" + Ch.ID);
        }

        /// <summary>
        /// This Method Unarchives a channel.
        /// The calling user is added to the channel.
        /// </summary>
        /// <param name="ChannelID">ID of channel to unarchive.</param>
        /// <returns>Result of unarchiving.</returns>
        public async Task<Result> UnarchiveChannelAsync(string ChannelID)
        {
            return await SendRequestAsync<Result>("channels.unarchive", "channel=" + ChannelID);
        }
        #endregion

        #region chat.delete
        /// <summary>
        /// This method deletes a message from a channel.
        /// (Required scope: chat:write:bot or chat:write:user)
        /// </summary>
        /// <param name="Ch">Channel containing the message to be deleted.</param>
        /// <param name="Timestamp">Timestamp of the message to be deleted.</param>
        /// <param name="AsUser">Pass true to delete the message as the authed user.</param>
        /// <returns>Result of deleting message.</returns>
        public ChatResult DeleteMessage(Channel Ch, string Timestamp, bool AsUser = false)
        {
            return SendRequest<ChatResult>("chat.delete", "channel=" + Ch.ID + "&ts="
                                            + Timestamp + "&as_user=" + AsUser.ToString());
        }

        /// <summary>
        /// This method deletes a message from a channel.
        /// (Required scope: chat:write:bot or chat:write:user)
        /// </summary>
        /// <param name="ChannelID">ID of channel containing the message to be deleted.</param>
        /// <param name="Timestamp">Timestamp of the message to be deleted.</param>
        /// <param name="AsUser">Pass true to delete the message as the authed user.</param>
        /// <returns>Result of deleting message.</returns>
        public ChatResult DeleteMessage(string ChannelID, string Timestamp, bool AsUser = false)
        {
            return SendRequest<ChatResult>("chat.delete", "channel=" + ChannelID + "&ts="
                                            + Timestamp + "&as_user=" + AsUser.ToString());
        }

        /// <summary>
        /// This method deletes a message from a channel.
        /// (Required scope: chat:write:bot or chat:write:user)
        /// </summary>
        /// <param name="Ch">Channel containing the message to be deleted.</param>
        /// <param name="Timestamp">Timestamp of the message to be deleted.</param>
        /// <param name="AsUser">Pass true to delete the message as the authed user.</param>
        /// <returns>Result of deleting message.</returns>
        public async Task<ChatResult> DeleteMessageAsync(Channel Ch, string Timestamp, bool AsUser = false)
        {
            return await SendRequestAsync<ChatResult>("chat.delete", "channel=" + Ch.ID + "&ts="
                                                        + Timestamp + "&as_user=" + AsUser.ToString());
        }

        /// <summary>
        /// This method deletes a message from a channel.
        /// (Required scope: chat:write:bot or chat:write:user)
        /// </summary>
        /// <param name="ChannelID">ID of channel containing the message to be deleted.</param>
        /// <param name="Timestamp">Timestamp of the message to be deleted.</param>
        /// <param name="AsUser">Pass true to delete the message as the authed user.</param>
        /// <returns>Result of deleting message.</returns>
        public async Task<ChatResult> DeleteMessageAsync(string ChannelID, string Timestamp, bool AsUser = false)
        {
            return await SendRequestAsync<ChatResult>("chat.delete", "channel=" + ChannelID + "&ts="
                                                        + Timestamp + "&as_user=" + AsUser.ToString());
        }
        #endregion
        #region chat.meMessage
        /// <summary>
        /// This method sends a me message to a channel from the calling user.
        /// (Required scope: chat:write:user)
        /// </summary>
        /// <param name="Ch">Channel to send message to.</param>
        /// <param name="Text">Text of the message to send.</param>
        /// <returns>Result of posting me message.</returns>
        public ChatResult MeMessage(Channel Ch, string Text)
        {
            return SendRequest<ChatResult>("chat.meMessage", "channel=" + Ch.ID + "&text=" + Text);
        }

        /// <summary>
        /// This method sends a me message to a channel from the calling user.
        /// (Required scope: chat:write:user)
        /// </summary>
        /// <param name="ChannelID">ID of channel to send message to.</param>
        /// <param name="Text">Text of the message to send.</param>
        /// <returns>Result of posting me message.</returns>
        public ChatResult MeMessage(string ChannelID, string Text)
        {
            return SendRequest<ChatResult>("chat.meMessage", "channel=" + ChannelID + "&text=" + Text);
        }

        /// <summary>
        /// This method sends a me message to a channel from the calling user.
        /// (Required scope: chat:write:user)
        /// </summary>
        /// <param name="Ch">Channel to send message to.</param>
        /// <param name="Text">Text of the message to send.</param>
        /// <returns>Result of posting me message.</returns>
        public async Task<ChatResult> MeMessageAsync(Channel Ch, string Text)
        {
            return await SendRequestAsync<ChatResult>("chat.meMessage", "channel=" + Ch.ID + "&text=" + Text);
        }

        /// <summary>
        /// This method sends a me message to a channel from the calling user.
        /// (Required scope: chat:write:user)
        /// </summary>
        /// <param name="ChannelID">ID of channel to send message to.</param>
        /// <param name="Text">Text of the message to send.</param>
        /// <returns>Result of posting me message.</returns>
        public async Task<ChatResult> MeMessageAsync(string ChannelID, string Text)
        {
            return await SendRequestAsync<ChatResult>("chat.meMessage", "channel=" + ChannelID + "&text=" + Text);
        }
        #endregion
        #region chat.postMessage
        /// <summary>
        /// This method posts a message to a public channel,
        /// private channel, or direct message/IM channel.
        /// (Required scope: chat:write:bot or chat:write:user)
        /// </summary>
        /// <param name="Ch">Channel, private group, or IM channel to send message to.</param>
        /// <param name="Text">Text of the message to send. </param>
        /// <param name="AsUser">
        ///     Pass true to post the message as the authed user, instead of as a bot.
        ///     If this parameter be setted true, UserName, IconUrl and IconEmoji will be ignored.
        /// </param>
        /// <param name="UserName">Your bot's user name. Must be used in conjunction with AsUser set to false.</param>
        /// <param name="IconUrl">URL to an image to use as the icon for this message.</param>
        /// <param name="IconEmoji">emoji to use as the icon for this message.Overrides IconUrl.</param>
        /// <param name="Attachments">Structured message attachments.</param>
        /// <param name="Parse">Change how messages are treated.</param>
        /// <param name="LinkNames">Find and link channel names and usernames.</param>
        /// <param name="UnfurlLinks">Pass true to enable unfurling of primarily text-based content.</param>
        /// <param name="UnfurlMedia">Pass false to disable unfurling of media content.</param>
        /// <returns>Result of posting message.</returns>
        public ChatResult PostMessage(Channel Ch, string Text, bool AsUser = false,
                                      string UserName = null, string IconUrl = null, string IconEmoji = null,
                                      Attachment[] Attachments = null, ParseType Parse = ParseType.none,
                                      bool LinkNames = true, bool UnfurlLinks = true, bool UnfurlMedia = false)
        {
            string StrLinkNames = LinkNames ? "1" : "none";
            string Strings = "";

            if (UserName != null) Strings += "&username=" + UserName;
            if (IconUrl != null) Strings += "&icon_url=" + IconUrl;
            if (IconEmoji != null) Strings += "&icon_emoji=" + IconEmoji;

            string StrAttachments =
                JsonConvert.SerializeObject(Attachments, Formatting.None,
                                            new JsonSerializerSettings {
                                                DefaultValueHandling = DefaultValueHandling.Ignore
                                            });

            return SendRequest<ChatResult>("chat.postMessage", "channel=" + Ch.ID + "&text=" + Text
                                         + "&as_user=" + AsUser.ToString() + Strings
                                         + "&attachments=" + StrAttachments
                                         + "&parse=" + Parse.ToString()
                                         + "&link_names=" + StrLinkNames
                                         + "&unfurl_links=" + UnfurlLinks.ToString()
                                         + "&unfurl_media=" + UnfurlMedia.ToString());
        }

        /// <summary>
        /// This method posts a message to a public channel,
        /// private channel, or direct message/IM channel.
        /// (Required scope: chat:write:bot or chat:write:user)
        /// </summary>
        /// <param name="ChannelID">ID or name of channel, private group, or IM channel to send message to.</param>
        /// <param name="Text">Text of the message to send. </param>
        /// <param name="AsUser">
        ///     Pass true to post the message as the authed user, instead of as a bot.
        ///     If this parameter be setted true, UserName, IconUrl and IconEmoji will be ignored.
        /// </param>
        /// <param name="UserName">Your bot's user name. Must be used in conjunction with AsUser set to false.</param>
        /// <param name="IconUrl">URL to an image to use as the icon for this message.</param>
        /// <param name="IconEmoji">emoji to use as the icon for this message.Overrides IconUrl.</param>
        /// <param name="Attachments">Structured message attachments.</param>
        /// <param name="Parse">Change how messages are treated.</param>
        /// <param name="LinkNames">Find and link channel names and usernames.</param>
        /// <param name="UnfurlLinks">Pass true to enable unfurling of primarily text-based content.</param>
        /// <param name="UnfurlMedia">Pass false to disable unfurling of media content.</param>
        /// <returns>Result of posting message.</returns>
        public ChatResult PostMessage(string ChannelID, string Text, bool AsUser = false,
                                      string UserName = null, string IconUrl = null, string IconEmoji = null,
                                      Attachment[] Attachments = null, ParseType Parse = ParseType.none,
                                      bool LinkNames = true, bool UnfurlLinks = true, bool UnfurlMedia = false)
        {
            string StrLinkNames = LinkNames ? "1" : "none";
            string Strings = "";

            if (UserName != null) Strings += "&username=" + UserName;
            if (IconUrl != null) Strings += "&icon_url=" + IconUrl;
            if (IconEmoji != null) Strings += "&icon_emoji=" + IconEmoji;

            string StrAttachments =
                JsonConvert.SerializeObject(Attachments, Formatting.None,
                                            new JsonSerializerSettings
                                            {
                                                DefaultValueHandling = DefaultValueHandling.Ignore
                                            });

            return SendRequest<ChatResult>("chat.postMessage", "channel=" + ChannelID + "&text=" + Text
                                         + "&as_user=" + AsUser.ToString() + Strings
                                         + "&attachments=" + StrAttachments
                                         + "&parse=" + Parse.ToString()
                                         + "&link_names=" + StrLinkNames
                                         + "&unfurl_links=" + UnfurlLinks.ToString()
                                         + "&unfurl_media=" + UnfurlMedia.ToString());
        }

        /// <summary>
        /// This method posts a message to a public channel,
        /// private channel, or direct message/IM channel.
        /// (Required scope: chat:write:bot or chat:write:user)
        /// </summary>
        /// <param name="Ch">Channel, private group, or IM channel to send message to.</param>
        /// <param name="Text">Text of the message to send. </param>
        /// <param name="AsUser">
        ///     Pass true to post the message as the authed user, instead of as a bot.
        ///     If this parameter be setted true, UserName, IconUrl and IconEmoji will be ignored.
        /// </param>
        /// <param name="UserName">Your bot's user name. Must be used in conjunction with AsUser set to false.</param>
        /// <param name="IconUrl">URL to an image to use as the icon for this message.</param>
        /// <param name="IconEmoji">emoji to use as the icon for this message.Overrides IconUrl.</param>
        /// <param name="Attachments">Structured message attachments.</param>
        /// <param name="Parse">Change how messages are treated.</param>
        /// <param name="LinkNames">Find and link channel names and usernames.</param>
        /// <param name="UnfurlLinks">Pass true to enable unfurling of primarily text-based content.</param>
        /// <param name="UnfurlMedia">Pass false to disable unfurling of media content.</param>
        /// <returns>Result of posting message.</returns>
        public async Task<ChatResult> PostMessageAsync(Channel Ch, string Text, bool AsUser = false,
                                                       string UserName = null, string IconUrl = null, string IconEmoji = null,
                                                       Attachment[] Attachments = null, ParseType Parse = ParseType.none,
                                                       bool LinkNames = true, bool UnfurlLinks = true, bool UnfurlMedia = false)
        {
            string StrLinkNames = LinkNames ? "1" : "none";
            string Strings = "";

            if (UserName != null) Strings += "&username=" + UserName;
            if (IconUrl != null) Strings += "&icon_url=" + IconUrl;
            if (IconEmoji != null) Strings += "&icon_emoji=" + IconEmoji;

            string StrAttachments = await Task.Factory.StartNew(() =>
                JsonConvert.SerializeObject(Attachments, Formatting.None,
                                            new JsonSerializerSettings
                                            {
                                                DefaultValueHandling = DefaultValueHandling.Ignore
                                            }));

            return await SendRequestAsync<ChatResult>("chat.postMessage", "channel=" + Ch.ID + "&text=" + Text
                                                    + "&as_user=" + AsUser.ToString() + Strings
                                                    + "&attachments=" + StrAttachments
                                                    + "&parse=" + Parse.ToString()
                                                    + "&link_names=" + StrLinkNames
                                                    + "&unfurl_links=" + UnfurlLinks.ToString()
                                                    + "&unfurl_media=" + UnfurlMedia.ToString());
        }

        /// <summary>
        /// This method posts a message to a public channel,
        /// private channel, or direct message/IM channel.
        /// (Required scope: chat:write:bot or chat:write:user)
        /// </summary>
        /// <param name="ChannelID">ID or name of channel, private group, or IM channel to send message to.</param>
        /// <param name="Text">Text of the message to send. </param>
        /// <param name="AsUser">
        ///     Pass true to post the message as the authed user, instead of as a bot.
        ///     If this parameter be setted true, UserName, IconUrl and IconEmoji will be ignored.
        /// </param>
        /// <param name="UserName">Your bot's user name. Must be used in conjunction with AsUser set to false.</param>
        /// <param name="IconUrl">URL to an image to use as the icon for this message.</param>
        /// <param name="IconEmoji">emoji to use as the icon for this message.Overrides IconUrl.</param>
        /// <param name="Attachments">Structured message attachments.</param>
        /// <param name="Parse">Change how messages are treated.</param>
        /// <param name="LinkNames">Find and link channel names and usernames.</param>
        /// <param name="UnfurlLinks">Pass true to enable unfurling of primarily text-based content.</param>
        /// <param name="UnfurlMedia">Pass false to disable unfurling of media content.</param>
        /// <returns>Result of posting message.</returns>
        public async Task<ChatResult> PostMessageAsync(string ChannelID, string Text, bool AsUser = false,
                                                       string UserName = null, string IconUrl = null, string IconEmoji = null,
                                                       Attachment[] Attachments = null, ParseType Parse = ParseType.none,
                                                       bool LinkNames = true, bool UnfurlLinks = true, bool UnfurlMedia = false)
        {
            string StrLinkNames = LinkNames ? "1" : "none";
            string Strings = "";

            if (UserName != null) Strings += "&username=" + UserName;
            if (IconUrl != null) Strings += "&icon_url=" + IconUrl;
            if (IconEmoji != null) Strings += "&icon_emoji=" + IconEmoji;

            string StrAttachments = await Task.Factory.StartNew(() =>
                JsonConvert.SerializeObject(Attachments, Formatting.None,
                                            new JsonSerializerSettings
                                            {
                                                DefaultValueHandling = DefaultValueHandling.Ignore
                                            }));

            return await SendRequestAsync<ChatResult>("chat.postMessage", "channel=" + ChannelID + "&text=" + Text
                                                    + "&as_user=" + AsUser.ToString() + Strings
                                                    + "&attachments=" + StrAttachments
                                                    + "&parse=" + Parse.ToString()
                                                    + "&link_names=" + StrLinkNames
                                                    + "&unfurl_links=" + UnfurlLinks.ToString()
                                                    + "&unfurl_media=" + UnfurlMedia.ToString());
        }
        #endregion
        #region chat.update
        /// <summary>
        /// This method updates a message in a channel.
        /// Through related to PostMessage,
        /// some parameters of UpdateMessage are handled differently.
        /// (Required scope: chat:write:bot or chat:write:user)
        /// </summary>
        /// <param name="Ch">Channel containing the message to be updated.</param>
        /// <param name="Timestamp">Timestamp of the message to be updated.</param>
        /// <param name="Text">New text for the message.</param>
        /// <param name="AsUser">Pass true to update the message as the authed user.</param>
        /// <param name="LinkNames">Find and link channel names and usernames.</param>
        /// <param name="Attachments">Structured message attachments.</param>
        /// <param name="Parse">Change how messages are treated.</param>
        /// <returns>Result of updating message.</returns>
        public ChatResult UpdateMessage(Channel Ch, string Timestamp, string Text, bool AsUser = false,
                                        bool LinkNames = true, ParseType Parse = ParseType.client,
                                        Attachment[] Attachments = null)
        {
            string StrLinkNames = LinkNames ? "1" : "none";
            string StrAttachments =
                JsonConvert.SerializeObject(Attachments, Formatting.None,
                                            new JsonSerializerSettings
                                            {
                                                DefaultValueHandling = DefaultValueHandling.Ignore
                                            });

            return SendRequest<ChatResult>("chat.postMessage", "channel=" + Ch.ID + "&text=" + Text
                                         + "&as_user=" + AsUser.ToString()
                                         + "&attachments=" + StrAttachments
                                         + "&parse=" + Parse.ToString()
                                         + "&link_names=" + StrLinkNames);
        }

        /// <summary>
        /// This method updates a message in a channel.
        /// Through related to PostMessage,
        /// some parameters of UpdateMessage are handled differently.
        /// (Required scope: chat:write:bot or chat:write:user)
        /// </summary>
        /// <param name="Ch">ID of channel containing the message to be updated.</param>
        /// <param name="Timestamp">Timestamp of the message to be updated.</param>
        /// <param name="Text">New text for the message.</param>
        /// <param name="AsUser">Pass true to update the message as the authed user.</param>
        /// <param name="LinkNames">Find and link channel names and usernames.</param>
        /// <param name="Attachments">Structured message attachments.</param>
        /// <param name="Parse">Change how messages are treated.</param>
        /// <returns>Result of updating message.</returns>
        public ChatResult UpdateMessage(string ChannelID, string Timestamp, string Text, bool AsUser = false,
                                        bool LinkNames = true, ParseType Parse = ParseType.client,
                                        Attachment[] Attachments = null)
        {
            string StrLinkNames = LinkNames ? "1" : "none";
            string StrAttachments =
                JsonConvert.SerializeObject(Attachments, Formatting.None,
                                            new JsonSerializerSettings
                                            {
                                                DefaultValueHandling = DefaultValueHandling.Ignore
                                            });

            return SendRequest<ChatResult>("chat.postMessage", "channel=" + ChannelID + "&text=" + Text
                                         + "&as_user=" + AsUser.ToString()
                                         + "&attachments=" + StrAttachments
                                         + "&parse=" + Parse.ToString()
                                         + "&link_names=" + StrLinkNames);
        }

        /// <summary>
        /// This method updates a message in a channel.
        /// Through related to PostMessage,
        /// some parameters of UpdateMessage are handled differently.
        /// (Required scope: chat:write:bot or chat:write:user)
        /// </summary>
        /// <param name="Ch">Channel containing the message to be updated.</param>
        /// <param name="Timestamp">Timestamp of the message to be updated.</param>
        /// <param name="Text">New text for the message.</param>
        /// <param name="AsUser">Pass true to update the message as the authed user.</param>
        /// <param name="LinkNames">Find and link channel names and usernames.</param>
        /// <param name="Attachments">Structured message attachments.</param>
        /// <param name="Parse">Change how messages are treated.</param>
        /// <returns>Result of updating message.</returns>
        public async Task<ChatResult> UpdateMessageAsync(Channel Ch, string Timestamp, string Text, bool AsUser = false,
                                                         bool LinkNames = true, ParseType Parse = ParseType.client,
                                                         Attachment[] Attachments = null)
        {
            string StrLinkNames = LinkNames ? "1" : "none";
            string StrAttachments = await Task.Factory.StartNew(() =>
                JsonConvert.SerializeObject(Attachments, Formatting.None,
                                            new JsonSerializerSettings
                                            {
                                                DefaultValueHandling = DefaultValueHandling.Ignore
                                            }));

            return await SendRequestAsync<ChatResult>("chat.postMessage", "channel=" + Ch.ID + "&text=" + Text
                                                    + "&as_user=" + AsUser.ToString()
                                                    + "&attachments=" + StrAttachments
                                                    + "&parse=" + Parse.ToString()
                                                    + "&link_names=" + StrLinkNames);
        }

        /// <summary>
        /// This method updates a message in a channel.
        /// Through related to PostMessage,
        /// some parameters of UpdateMessage are handled differently.
        /// (Required scope: chat:write:bot or chat:write:user)
        /// </summary>
        /// <param name="Ch">ID of channel containing the message to be updated.</param>
        /// <param name="Timestamp">Timestamp of the message to be updated.</param>
        /// <param name="Text">New text for the message.</param>
        /// <param name="AsUser">Pass true to update the message as the authed user.</param>
        /// <param name="LinkNames">Find and link channel names and usernames.</param>
        /// <param name="Attachments">Structured message attachments.</param>
        /// <param name="Parse">Change how messages are treated.</param>
        /// <returns>Result of updating message.</returns>
        public async Task<ChatResult> UpdateMessageAsync(string ChannelID, string Timestamp, string Text, bool AsUser = false,
                                        bool LinkNames = true, ParseType Parse = ParseType.client,
                                        Attachment[] Attachments = null)
        {
            string StrLinkNames = LinkNames ? "1" : "none";
            string StrAttachments = await Task.Factory.StartNew(() =>
                JsonConvert.SerializeObject(Attachments, Formatting.None,
                                            new JsonSerializerSettings
                                            {
                                                DefaultValueHandling = DefaultValueHandling.Ignore
                                            }));

            return await SendRequestAsync<ChatResult>("chat.postMessage", "channel=" + ChannelID + "&text=" + Text
                                                    + "&as_user=" + AsUser.ToString()
                                                    + "&attachments=" + StrAttachments
                                                    + "&parse=" + Parse.ToString()
                                                    + "&link_names=" + StrLinkNames);
        }
        #endregion

        #region dnd.endDnd
        /// <summary>
        /// Ends the user's currently scheduled Do Not Disturb session immediately.
        /// (Required scope: dnd:write)
        /// </summary>
        /// <returns>Result of the action.</returns>
        public Result EndDnd()
        {
            return SendRequest<Result>("dnd.endDnd");
        }

        /// <summary>
        /// Ends the user's currently scheduled Do Not Disturb session immediately.
        /// (Required scope: dnd:write)
        /// </summary>
        /// <returns>Result of the action.</returns>
        public async Task<Result> EndDndAsync()
        {
            return await SendRequestAsync<Result>("dnd.endDnd");
        }
        #endregion
        #region dnd.endSnooze
        /// <summary>
        /// Ends the current user's snooze mode immediately.
        /// (Required scope: dnd:write)
        /// </summary>
        /// <returns>Current status for the user's Dnd.</returns>
        public DndResult EndSnooze()
        {
            return SendRequest<DndResult>("dnd.endSnooze");
        }
        
        /// <summary>
        /// Ends the current user's snooze mode immediately.
        /// (Required scope: dnd:write)
        /// </summary>
        /// <returns>Current status for the user's Dnd.</returns>
        public async Task<DndResult> EndSnoozeAsync()
        {
            return await SendRequestAsync<DndResult>("dnd.endSnooze");
        }
        #endregion
        #region dnd.info
        /// <summary>
        /// Provides information about a user's current Dnd settings.
        /// (Required scope: dnd:read)
        /// </summary>
        /// <param name="U">User to fetch status for. (defaults to current user)</param>
        /// <returns></returns>
        public DndResult GetDndInfo(User U = null)
        {
            return U == null ?
                    SendRequest<DndResult>("dnd.info")
                  : SendRequest<DndResult>("dnd.info", "user=" + U.ID);
        }

        /// <summary>
        /// Provides information about a user's current Dnd settings.
        /// (Required scope: dnd:read)
        /// </summary>
        /// <param name="UserID">ID of user to fetch status for. (defaults to current user)</param>
        /// <returns></returns>
        public DndResult GetDndInfo(string UserID = null)
        {
            return UserID == null ?
                    SendRequest<DndResult>("dnd.info")
                  : SendRequest<DndResult>("dnd.info", "user=" + UserID);
        }

        /// <summary>
        /// Provides information about a user's current Dnd settings.
        /// (Required scope: dnd:read)
        /// </summary>
        /// <param name="U">User to fetch status for. (defaults to current user)</param>
        /// <returns></returns>
        public async Task<DndResult> GetDndInfoAsync(User U = null)
        {
            return U == null ?
                    await SendRequestAsync<DndResult>("dnd.info")
                  : await SendRequestAsync<DndResult>("dnd.info", "user=" + U.ID);
        }

        /// <summary>
        /// Provides information about a user's current Dnd settings.
        /// (Required scope: dnd:read)
        /// </summary>
        /// <param name="UserID">ID of user to fetch status for. (defaults to current user)</param>
        /// <returns></returns>
        public async Task<DndResult> GetDndInfoAsync(string UserID = null)
        {
            return UserID == null ?
                    await SendRequestAsync<DndResult>("dnd.info")
                  : await SendRequestAsync<DndResult>("dnd.info", "user=" + UserID);
        }
        #endregion
        #region dnd.setSnooze
        /// <summary>
        /// Adjusts the snooze duration for a user's Dnd settings.
        /// If a snooze session is not already active for user,
        /// invoking this method will begin one for the specified duration.
        /// (Required scope: dnd:write)
        /// </summary>
        /// <param name="Minutes">Minutes from now to snooze until.</param>
        /// <returns>Current status for the user's Dnd.</returns>
        public DndResult SetSnooze(int Minutes)
        {
            return SendRequest<DndResult>("dnd.setSnooze", "num_minutes=" + Minutes);
        }

        /// <summary>
        /// Adjusts the snooze duration for a user's Dnd settings.
        /// If a snooze session is not already active for user,
        /// invoking this method will begin one for the specified duration.
        /// (Required scope: dnd:write)
        /// </summary>
        /// <param name="Minutes">Minutes from now to snooze until.</param>
        /// <returns>Current status for the user's Dnd.</returns>
        public async Task<DndResult> SetSnoozeAsync(int Minutes)
        {
            return await SendRequestAsync<DndResult>("dnd.setSnooze", "num_minutes=" + Minutes);
        }
        #endregion

        #region groups.archive
        public Result ArchiveGroup(string channel)
        {
            return SendRequest<Result>("groups.archive", "channel=" + channel);
        }

        public async Task<Result> ArchiveGroupAsync(string channel)
        {
            return await SendRequestAsync<Result>("groups.archive", "channel=" + channel);
        }
        #endregion
        #region groups.close
        public GroupCloseResult CloseGroup(string channel)
        {
            return SendRequest<GroupCloseResult>("groups.close", "channel=" + channel);
        }

        public async Task<GroupCloseResult> CloseGroupAsync(string channel)
        {
            return await SendRequestAsync<GroupCloseResult>("groups.close", "channel=" + channel);
        }
        #endregion
        #region groups.create
        public GroupResult CreateGroup(string name)
        {
            return SendRequest<GroupResult>("groups.create", "name=" + name);
        }

        public async Task<GroupResult> CreateGroupAsync(string name)
        {
            return await SendRequestAsync<GroupResult>("groups.create", "name=" + name);
        }
        #endregion
    }
}