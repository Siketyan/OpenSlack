﻿using Newtonsoft.Json;

namespace OpenSlack
{
    public class Channel
    {
        [JsonProperty("id")]
        public string ID { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("created")]
        public long Created { get; set; }

        [JsonProperty("creator")]
        public string Creator { get; set; }

        [JsonProperty("is_archived")]
        public bool IsArchived { get; set; }

        [JsonProperty("is_member")]
        public bool IsMember { get; set; }

        [JsonProperty("is_general")]
        public bool IsGeneral { get; set; }

        /// <summary>
        /// This will be null if called by GetChannels() method.
        /// </summary>
        [JsonProperty("last_read")]
        public string LastRead { get; set; }

        /// <summary>
        /// This will be null if the channel is got by GetChannels method.
        /// </summary>
        [JsonProperty("latest")]
        public Message Latest { get; set; }

        /// <summary>
        /// This will be 0 if the channel is got by GetChannels method.
        /// </summary>
        [JsonProperty("unread_count")]
        public int Unreads { get; set; }

        /// <summary>
        /// This will be 0 if the channel is got by GetChannels method.
        /// </summary>
        [JsonProperty("unread_count_display")]
        public int UnreadsDisplay { get; set; }

        /// <summary>
        /// This will be null if the channel is got by GetChannels method.
        /// </summary>
        [JsonProperty("members")]
        public string[] Members { get; set; }

        /// <summary>
        /// This will be 0 if the channel is got by CreateChannel or GetChannel method.
        /// </summary>
        [JsonProperty("num_members")]
        public int NumMembers { get; set; }

        [JsonProperty("topic")]
        public Subject Topic { get; set; }

        [JsonProperty("purpose")]
        public Subject Purpose { get; set; }
    }

    public class Subject
    {
        [JsonProperty("value")]
        public string Value { get; set; }

        [JsonProperty("creator")]
        public string Creator { get; set; }

        [JsonProperty("last_set")]
        public long LastSet { get; set; }
    }
}