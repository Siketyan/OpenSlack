﻿using Newtonsoft.Json;

namespace OpenSlack
{
    public class BotResult : Result
    {
        [JsonProperty("bot")]
        public Bot Bot { get; set; }
    }

    public class Bot
    {
        [JsonProperty("id")]
        public string ID { get; set; }

        [JsonProperty("deleted")]
        public bool IsDeleted { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("icons")]
        public Icons Icon { get; set; }
    }

    public class Icons
    {
        [JsonProperty("image_36")]
        public string Image36 { get; set; }

        [JsonProperty("image_48")]
        public string Image48 { get; set; }

        [JsonProperty("image_72")]
        public string Image72 { get; set; }
    }
}
