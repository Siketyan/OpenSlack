﻿using Newtonsoft.Json;

namespace OpenSlack
{
    public class RevokeResult : Result
    {
        [JsonProperty("revoked")]
        public bool IsRevoked { get; set; }
    }
}
