﻿using System;

namespace OpenSlack
{
    public class SlackException : Exception
    {
        public string ErrorCode { get; private set; }

        public SlackException(string ErrorCode)
            : base(ErrorCode + " : " + GetMessage(ErrorCode))
        {
            this.ErrorCode = ErrorCode;
        }

        private static string GetMessage(string ErrorCode)
        {
            switch (ErrorCode)
            {
                case "account_inactive": return "Authentication token is for a deleted user or team.";
                case "already_archived": return "Channel has already been archived.";
                case "already_in_channel": return "Invited user is already in the channel.";
                case "bot_not_found": return "Bot ID was invalid.";
                case "cant_archive_general": return "You cannot archive the general channel.";
                case "cant_delete_message": return "Authenticated user does not have permission to delete this message.";
                case "cant_invite": return "User cannot be invited to this channel.";
                case "cant_invite_self": return "Authenticated user cannot invite themselves to a channel.";
                case "cant_kick_from_general": return "User cannot be removed from #general.";
                case "cant_kick_from_last_channel": return "User cannot be removed from the last channel they're in.";
                case "cant_kick_self": return "Authenticated user can't kick themselves from a channel.";
                case "cant_leave_general": return "Authenticated user cannot leave the general channel.";
                case "cant_update_message": return "Authenticated user does not have permission to update this message.";
                case "channel_not_found": return "Channel ID was invalid.";
                case "compliance_exports_prevent_deletion": return "Compliance exports are on, messages can not be deleted.";
                case "edit_window_closed": return "The message cannot be edited due to the team message edit settings.";
                case "invalid_auth": return "Invalid authentication token.";
                case "invalid_arg_name": return "The method was passed an argument whose name falls outside the bounds of common decency. This includes very long names and names with non-alphanumeric characters other than _. If you get this error, it is typically an indication that you have made a very malformed API call.";
                case "invalid_array_arg": return "The method was passed a PHP-style array argument (e.g. with a name like foo[7]). These are never valid with the Slack API.";
                case "invalid_timestamp": return "Timestamp was invalid.";
                case "invalid_ts_latest": return "Latest timestamp was invalid.";
                case "invalid_ts_oldest": return "Oldest timestamp was invalid.";
                case "is_archived": return "Channel has been archived.";
                case "last_ra_channel": return "You cannot archive the last channel for a multi-channel guest.";
                case "message_not_found": return "No messages exists with the requested timestamp.";
                case "missing_duration": return "No value provided for Minutes.";
                case "msg_too_long": return "Message text is too long.";
                case "name_taken": return "Name of channel to create or rename is taken.";
                case "not_archived": return "Channel is not archived.";
                case "not_authed": return "No authentication token provided.";
                case "not_authorized": return "No authentication token provided.";
                case "not_in_channel": return "Caller is not in the channel.";
                case "no_channel": return "Name of channel to create or rename was empty.";
                case "no_text": return "No message text provided.";
                case "rate_limited": return "Application has posted too many messages.";
                case "restricted_action": return "A team preference prevents the authenticated user from the action.";
                case "snooze_end_failed": return "There was a problem setting the user's Dnd status.";
                case "snooze_failed": return "There was a problem setting the user's Dnd status.";
                case "snooze_not_active": return "Snooze is not active for the user and cannot be ended.";
                case "too_long": return "Purpose or topic was longer than 250 characters.";
                case "too_many_attachments": return "Too many attachments were provided with this message. A maximum of 100 attachments are allowed on a message.";
                case "too_many_users": return "Too many users sent in the users arg. (max 30)";
                case "unknown_error": return "There was a mysterious problem in the action.";
                case "user_is_bot": return "This method cannnot be called by a bot user.";
                case "user_is_restricted": return "This method cannot be called by a restricted user or single channel guest.";
                case "user_is_ultra_restricted": return "This method cannot be called by a single channel guest.";
                case "user_not_found": return "User ID was invalid.";
                
                default: return "Unknown error.";
            }
        }
    }
}