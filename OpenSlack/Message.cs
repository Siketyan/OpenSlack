﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace OpenSlack
{
    public class Message
    {
        /// <summary>
        /// Messages of type "message" are user-entered text messages sent to the channel,
        /// while other types are events that happened within the channel. 
        /// </summary>
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("ts")]
        public string Timestamp { get; set; }

        [JsonProperty("user")]
        public string UserID { get; set; }

        [JsonProperty("text")]
        public string Text { get; set; }
        
        [JsonProperty("is_starred")]
        public bool IsStarred { get; set; }

        /// <summary>
        /// This will be null if the message doesn't have any reactions.
        /// </summary>
        [JsonProperty("reactions")]
        public List<Reaction> Reactions { get; set; }
    }

    public class Reaction
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("count")]
        public int Count { get; set; }

        [JsonProperty("users")]
        public string[] Users { get; set; }
    }
}
