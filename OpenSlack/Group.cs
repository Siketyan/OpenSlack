﻿using Newtonsoft.Json;

namespace OpenSlack
{
    public class Group
    {
        [JsonProperty("id")]
        public string ID { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

		[JsonProperty("is_group")]
		public bool IsGroup { get; set; }

		[JsonProperty("created")]
        public long Created { get; set; }

        [JsonProperty("creator")]
        public string Creator { get; set; }

        [JsonProperty("is_archived")]
        public bool IsArchived { get; set; }

		[JsonProperty("is_open")]
		public bool IsOpen { get; set; }

        /// <summary>
        /// This will be null if called by GetChannels() method.
        /// </summary>
        [JsonProperty("last_read")]
        public string LastRead { get; set; }

        /// <summary>
        /// This will be null if the channel is got by GetChannels method.
        /// </summary>
        [JsonProperty("latest")]
        public Message Latest { get; set; }

        /// <summary>
        /// This will be 0 if the channel is got by GetChannels method.
        /// </summary>
        [JsonProperty("unread_count")]
        public int Unreads { get; set; }

        /// <summary>
        /// This will be 0 if the channel is got by GetChannels method.
        /// </summary>
        [JsonProperty("unread_count_display")]
        public int UnreadsDisplay { get; set; }

        /// <summary>
        /// This will be null if the channel is got by GetChannels method.
        /// </summary>
        [JsonProperty("members")]
        public string[] Members { get; set; }

  

        [JsonProperty("topic")]
        public Subject Topic { get; set; }

        [JsonProperty("purpose")]
        public Subject Purpose { get; set; }
    }

    
}