﻿using Newtonsoft.Json;

namespace OpenSlack
{
    public class User : Result
    {
        [JsonProperty("user_id")]
        public string ID { get; set; }

        [JsonProperty("url")]
        public string TeamURL { get; set; }

        [JsonProperty("team")]
        public string TeamName { get; set; }

        [JsonProperty("user")]
        public string Name { get; set; }

        [JsonProperty("team_id")]
        public string TeamID { get; set; }
    }
}