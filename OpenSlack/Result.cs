﻿using Newtonsoft.Json;

namespace OpenSlack
{
    public class Result
    {
        public string Json { get; set; }

        [JsonProperty("ok")]
        public bool IsOK { get; set; }

        [JsonProperty("error")]
        public string Error { get; set; }

        /// <summary>
        /// This will be used only by JoinToChannel method.
        /// </summary>
        [JsonProperty("already_in_channel")]
        public bool AlreadyInChannel { get; set; }

        /// <summary>
        /// This will be used only by SetChannelPurpose and SetGroupPurpose method.
        /// </summary>
        [JsonProperty("purpose")]
        public string Purpose { get; set; }

        /// <summary>
        /// This will be used only by SetChannelTopic and SetGroupTopic method.
        /// </summary>
        [JsonProperty("topic")]
        public string Topic { get; set; }
    }
}