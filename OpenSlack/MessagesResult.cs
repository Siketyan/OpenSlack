﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace OpenSlack
{
    public class MessagesResult : Result
    {
        [JsonProperty("latest")]
        public string Latest { get; set; }

        [JsonProperty("messages")]
        public List<Message> Messages { get; set; }

        [JsonProperty("has_more")]
        public bool HasMore { get; set; }
    }
}