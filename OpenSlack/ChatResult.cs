﻿using Newtonsoft.Json;

namespace OpenSlack
{
    public class ChatResult : Result
    {
        [JsonProperty("channel")]
        public string ChannelID { get; set; }

        [JsonProperty("ts")]
        public string Timestamp { get; set; }

        /// <summary>
        /// This will be setted only by PostMessage method.
        /// </summary>
        [JsonProperty("message")]
        public Message Message { get; set; }

        /// <summary>
        /// This will be setted only by UpdateMessage method.
        /// </summary>
        [JsonProperty("text")]
        public string Text { get; set; }
    }
}